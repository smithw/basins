{-# LANGUAGE GADTs, FlexibleInstances, FlexibleContexts, TypeOperators #-}

module Main where

import Vec
import System.IO
import Control.Monad
import Control.Monad.State
import Control.Monad.IO.Class
import Control.Parallel
import Control.Parallel.Strategies
import Data.List
import Data.Maybe

import qualified Data.Map as M

import Graphics.Rendering.Chart 
import Graphics.Rendering.Chart.Gtk
import Data.Colour 
import Data.Accessor



-- Dados de entrada: a função a ser analisada, seu jacobiano e os zeros
-- pré-determinados
jacobian :: JR2xR2
jacobian = (j11 :. j12 :. VZero) :.
	(j21 :. j22 :. VZero) :. VZero
	where
		j11 (x :. _ :. VZero) = 2*x
		j12 (_ :. y :. VZero) = -3*y**2
		j21 (x :. y :. VZero) = 2 * x * sin y
		j22 (x :. y :. VZero) = (x**2 - 1) * cos y

function :: R2xR2
function = f1 :. f2 :. VZero
	where
		f1 (x :. y :. VZero) = x**2 - y**3
		f2 (x :. y :. VZero) = (x**2 - 1) * sin y

zeroes :: [R2]
zeroes = [0.0 :. 0.0 :. VZero,
	1.0 :. 1.0 :. VZero,
	sqrt ((2 * pi) ** 3) :. 2 * pi :. VZero ]

functionName :: String
functionName = "f(x,y) = < x^2 - y^3, (x^2 - 1) * sen(y) >"

names :: [String]
names = ["<0.0, 0.0>",
	"<1.0, 1.0>",
	"<sqrt( (2pi)^3 ), 2pi>" ]

resolution :: Double
resolution = 0.1

rangeX :: (Double, Double)
rangeX = (-20.0, 20.0)

rangeY :: (Double, Double)
rangeY = (-20.0, 20.0)

acceptableError :: Double
acceptableError = 0.001

-- Classe que permite a comparação entre dois valores com um erro
-- pré-determinado
class EqError a where
	dist ::  a -> a -> Double
	eqErr ::  a -> a -> Double -> Bool
	eqErr x y err = dist x y < abs err
	(~==) :: (MonadState Double m) => a -> a -> m Bool
	x ~== y = liftM (eqErr x y) get

instance (IsNatural n) => EqError (R n) where
	dist = ((sqrt . vfoldr (+) 0) .) . vzipWith (\x_i y_i -> (x_i - y_i) ** 2)

solveLinearSystem :: (Fractional a, Ord a, IsNatural n, IsNatural (n :+ T1)) => a -> SMatrix n a -> Vec n a -> Maybe (Vec n a)
solveLinearSystem err coeffs free = let m = coeffs .>>+ free
	in diagonalize err m >>= lastCol

nextVector :: (MonadState Double m) => R2 -> m (Maybe R2)
nextVector x = do
	err <- get
	let
		coeffs = jacobian .$$ x -- aplica jacobiano a x
		prod = coeffs .** toColumn x -- multiplica resultado anterior pela matriz coluna x
		appliedFunc = function .$ x -- aplica a função original a x
	case prod .!! 0 of -- extrai a coluna do resultado do produto em um vetor
		Nothing -> return Nothing -- não deve acontecer
		Just prod' -> return $ solveLinearSystem err coeffs (prod' - appliedFunc)

run :: (MonadState Double m) => R2 -> m (Maybe Int)
run x = do
	next <- nextVector x
	case next of
		Nothing -> return Nothing
		Just x' -> do
			stop <- mapM (~== x') zeroes
			case True `elemIndex` stop of
				Just i -> return . Just $ i
				Nothing -> do
					conv <- x ~== x'
					if conv
						then return Nothing
						else run x'

toTuple :: R2 -> (Double, Double)
toTuple (x :. y :. VZero) = (x, y)

main :: IO ()
main = do
	let
		(x0, xf) = rangeX
		(y0, yf) = rangeY
		xRange = [x0,(x0 + resolution)..xf]
		yRange = [y0,(y0 + resolution)..yf]
		guesses = [ x :. y :. VZero | x <- xRange, y <- yRange]

		runGuesses = parMap rpar $ \guess -> let
			(r, _) = runState (run guess) acceptableError
			in case r of
				Just r' -> Just (guess, r')
				Nothing -> Nothing

		results = runGuesses guesses
		mkPoints (point, zeroIndex) = (zeroIndex, toTuple point)
		points = foldr (\(c, p) -> M.insertWith (++) c [p]) M.empty . map mkPoints . catMaybes $ results 

		charts = do
			key <- M.keys points
			let points' = M.lookup key points
			case points' of
				Just p -> return $ plot_points_values ^= p
					$ plot_points_style ^= filledCircles 1 (defaultColorSeq !! key)
					$ plot_points_title ^= "Bacia da raiz " ++ show (names !! key)
					$ defaultPlotPoints
				Nothing -> mzero

		layout = layout1_title ^= "Bacias de atração para " ++ functionName
			$ layout1_plots ^= map (Left . toPlot) charts
			$ defaultLayout1

	void $ renderableToPNGFile (toRenderable layout) 1024 768 "output.png"
	-- renderableToWindow (toRenderable layout) 1024 768
