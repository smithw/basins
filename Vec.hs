{-# LANGUAGE GADTs, DataKinds, TypeFamilies, TypeOperators, RankNTypes, FlexibleContexts, FlexibleInstances #-}

-- Fontes para implementação do tipo vetor:
-- 	http://stackoverflow.com/questions/12221080/applying-a-fixed-length-vector-function-to-the-inital-part-of-a-longer-fixed-len
-- 	http://www.haskell.org/pipermail/haskell/2005-May/015815.html

module Vec where

import Control.Monad
import qualified Data.List as L

-- Primeiro, definimos um tipo para representar números naturais em tipos
-- (não em variáveis diretamente). Isso é necessário para que possamos
-- restringir as dimensões utilizadas no próprio tipo das funções. Essa
-- declaração só é possível com a extensão DataKinds habilitada. Os números
-- naturais são definidos em função do zero e de seus respectivos
-- sucessores.

data TypeNatural = TZero | TSucc TypeNatural 

-- Apelidos para números naturais em tipo, para facilitar uso posterior.

type T1 = TSucc TZero
type T2 = TSucc T1
type T3 = TSucc T2
type T4 = TSucc T3
type T5 = TSucc T4

-- Classe que agrupa os tipos que são naturais e permite distinguir entre
-- os tipos TZero e não-TZero

class IsNatural n where
	select :: IsNaturalSelectable g => g n

class IsNaturalSelectable g where
	isZero :: g TZero
	isSuccessor :: (IsNatural n) => g (TSucc n)

instance IsNatural TZero where
	select = isZero

instance (IsNatural n) => IsNatural (TSucc n) where
	select = isSuccessor

-- Vec n a é o tipo que representa vetores de variáveis do tipo a com
-- dimensão n

data Vec n a where
	VZero :: Vec TZero a -- vetor com dimensão 0; não possui valor algum
	(:.) :: a -> Vec n a -> Vec (TSucc n) a -- "acrescenta" uma dimensão ao vetor

infixr 3 :.

type R n = Vec n Double
type R1 = R T1
type R2 = R T2
type R3 = R T3

vfoldr :: (a -> b -> b) -> b -> Vec n a -> b -- iteração
vfoldr _ z VZero = z
vfoldr f z (x :. xs) = f x (vfoldr f z xs)

-- aplica função a pares de elementos de vetore
vzipWith :: (a -> b -> c) -> Vec n a -> Vec n b -> Vec n c 
vzipWith _ VZero VZero = VZero
vzipWith f (x :. xs) (y :. ys) = f x y :. vzipWith f xs ys

(.*) :: Num a => Vec n a -> Vec n a -> a -- Produto interno
x .* y = vfoldr (+) 0 $ vzipWith (*) x y

-- Para lista simples
toList :: Vec n a -> [a]
toList = vfoldr (:) []

vlength :: Vec n a -> Int
vlength = vfoldr (const (+1)) 0

-- Define Vec n como sendo functor, permitindo que apliquemos uma função
-- a todos os elementos de um Vec
instance Functor (Vec n) where
	fmap _ VZero = VZero
	fmap f (x :. xs) = f x :. fmap f xs

-- Define Vec como sendo igualável
instance Eq a => Eq (Vec n a) where
	v1 == v2 = vfoldr (&&) True $ vzipWith (==) v1 v2

-- Define Vec como sendo mostrável
instance Show a => Show (Vec n a) where
	show = ("fromList " ++) . show . toList

-- Construtor de vetores com valores repetidos
newtype MkVec a n = MkVec { runMkVec :: a -> Vec n a }

vec :: IsNatural n => a -> Vec n a
vec = runMkVec select

instance IsNaturalSelectable (MkVec a) where
	isZero = MkVec $ const VZero
	isSuccessor = MkVec $ \x -> x :. vec x

-- Construtor a partir de listas; é necessário que a lista contenha
-- ao menos n elementos, como especificado pelo tipo de retorno Vec n a,
-- ou então a função retornará Nothing e nenhum vetor será criado.
newtype FromList a n = FromList { runFromList :: [a] -> Maybe (Vec n a) }

fromList :: IsNatural n => [a] -> Maybe (Vec n a)
fromList = runFromList select

instance IsNaturalSelectable (FromList a) where
	isZero = FromList . const $ Just VZero
	isSuccessor = FromList $ \l -> case l of
		x : xs -> fmap ((:.) x) $ fromList xs
		[] -> Nothing

-- Vec pertence à classe Num, porém a função (*) é enganosa; ela apenas faz
-- multiplicação item a item de um vetor, o que pode levar a resultados
-- incorretos. A função (.*) implementa corretamente o produto interno para
-- vetores e a função (.**) faz o mesmo para multiplicação de matrizes.

instance (IsNatural n, Num a) => Num (Vec n a) where
	(+) = vzipWith (+)
	(-) = vzipWith (-)
	(*) = vzipWith (*)
	negate = fmap negate
	abs = fmap abs
	signum = fmap signum
	fromInteger = vec . fromInteger

-- Define matrizes em função de vetores

type Matrix m n a = Vec m (Vec n a)
type SMatrix n a = Matrix n n a

mat :: (IsNatural n, IsNatural m) => a -> Matrix m n a
mat = vec . vec

fromLists :: (IsNatural m, IsNatural n) => [[a]] -> Maybe (Matrix m n a)
fromLists = (>>=fromList) . mapM fromList

toRow :: Vec n a -> Matrix T1 n a
toRow = vec

toColumn :: Vec n a -> Matrix n T1 a
toColumn = fmap (:.VZero)

transpose :: (IsNatural n) => Matrix m n a -> Matrix n m a
transpose VZero = vec VZero
transpose (x :. xs) = vzipWith (:.) x $ transpose xs

(.**) :: (IsNatural n, Num a) => Matrix m k a -> Matrix k n a -> Matrix m n a
m1 .** m2 = fmap (flip fmap m2' . (.*)) m1
	where m2' = transpose m2 

-- Matriz identidade
newtype IdMatrix a n = IdMatrix { runIdMatrix :: SMatrix n a }

idMatrix :: (IsNatural n, Num a) => SMatrix n a
idMatrix = runIdMatrix select

instance Num a => IsNaturalSelectable (IdMatrix a) where
	isZero = IdMatrix VZero
	isSuccessor = IdMatrix $ ((1 :. vec 0):.) (fmap (0:.) idMatrix)

-- Essa função retorna o enésimo elemento de um vetor, ou Nothing caso
-- o índice seja maior que o número de elementos menos 1. Ela também
-- funciona para retorna a LINHA de uma matriz; para a coluna, veja
-- a seguir.
(.!) :: Vec n a -> Int -> Maybe a
VZero .! _ = Nothing
(x :. _) .! 0 = Just x
(_ :. xs) .! n = xs .! (n - 1)

-- Retorna a enésima coluna de uma matriz
(.!!) :: (IsNatural m) => Matrix n m a -> Int -> Maybe (Vec n a)
(.!!) = (.!) . transpose

-- Define um monóide (grupo aditivo) no tipo TypeNatural
type family (n :: TypeNatural) :+ (m :: TypeNatural) :: TypeNatural
type instance TZero :+ n = n
type instance TSucc n :+ m = TSucc (n :+ m)

-- Adiciona um vetor ao final outro
(.>+) :: Vec n a -> Vec m a -> Vec (n :+ m) a
VZero .>+ ys = ys
(x :. xs) .>+ ys = x :. (xs .>+ ys)

vappend :: Vec n a -> a -> Vec (n :+ T1) a
vappend VZero y = vec y
vappend (x :. xs) y = x :. vappend xs y

-- Adiciona vetor ao final de matriz como coluna
(.>>+) :: Matrix m n a -> Vec m a -> Matrix m (n :+ T1) a
(.>>+) = vzipWith vappend

-- Adiciona vetor ao final de matrix como linha
(.\/+) :: Matrix m n a -> Vec n a -> Matrix (m :+ T1) n a
(.\/+) = vappend

-- Multiplicação de vetor por escalar
(..*) :: (Num a) => a -> Vec n a -> Vec n a
(..*) = fmap . (*)

-- Multiplicação de linha de matriz por escalar
multLine :: (Num a) => Int -> a -> Matrix m n a -> Matrix m n a
multLine _ _ VZero = VZero
multLine 0 s (x :. xs) = (s ..* x) :. xs
multLine n s (x :. xs) = x :. multLine (n - 1) s xs

-- Troca uma linha em uma matriz por um vetor arbitrário
replace :: Int -> Vec n a -> Matrix m n a -> Matrix m n a
replace _ _ VZero = VZero
replace 0 v (_ :. xs) = v :. xs
replace n v (x :. xs) = x :. replace (n-1) v xs

-- Troca de linhas em matriz
swap :: Int -> Int -> Matrix m n a -> Matrix m n a
swap i j m = maybe m id $ do
	li <- m .! i
	lj <- m .! j
	return . replace i lj . replace j li $ m

-- Multiplica uma linha por um escalar e a soma a outra linha em uma matriz
combine :: (Num a, IsNatural n) => Int -> Int -> a -> Matrix m n a -> Matrix m n a
combine i j s m = combine' (m .! i) j s m
	where
		combine' :: (Num a, IsNatural n) => Maybe (Vec n a) -> Int -> a -> Matrix m n a -> Matrix m n a
		combine' Nothing _ _ m' = m'
		combine' _ _ _ VZero = VZero
		combine' (Just src) 0 s' (x :. xs) = (sum' x) :. xs
			where sum' = (+) $ s' ..* src
		combine' src@(Just _) j' s' (x :. xs) = x :. combine' src (j'-1) s' xs
		
-- Retorna o último elemento de um vetor (útil para resolução de sistemas
-- lineares)
lastElem :: Vec n a -> Maybe a
lastElem VZero = Nothing
lastElem (x :. VZero)  = Just x
lastElem (_ :. xs) = lastElem xs

lastCol :: (IsNatural m) => Matrix n m a -> Maybe (Vec n a)
lastCol = lastElem . transpose

-- Diagonaliza uma matriz pelo seu lado esquerdo (útil para resolução de
-- sistemas lineares)
diagonalize :: (Fractional a, Ord a, IsNatural m, IsNatural n) => a -> Matrix n m a -> Maybe (Matrix n m a)
diagonalize err m = do
		m' <- stagger err m
		return . foldr (zeroAbove err) m' $ [(vlength m' - 1),(vlength m' - 2)..1]
	where
		zeroAbove err' i m' = foldr (zeroCompose err' i) m' [i-1,i-2..0]
		zeroCompose err' j i' m' = case m' .! i' >>= (.! j) of
			Nothing -> m'
			Just x -> if abs x < err'
				then m'
				else combine j i' (-x) m'

firstNotNull :: (Num a, Ord a) => a -> Int -> Vec n a -> Maybe (Int, a)
firstNotNull _ _ VZero = Nothing
firstNotNull err' j (x :. xs) = if abs x < err' then firstNotNull err' (j+1) xs else Just (j, x)

stagger :: (Fractional a, Ord a, IsNatural m, IsNatural n) => a -> Matrix n m a -> Maybe (Matrix n m a)
stagger err m = sortWithErr err m >>= stagger' err
	where
		zeroBelow err' (_ :. xs) j = snd . vfoldr (zeroCompose err' j) (vlength xs, id) $ xs
		zeroCompose err' j line (i', f) = case line .! j of
			Nothing -> (i' - 1, f) -- should never happen...
			Just x -> if abs x < err'
				then (i' - 1, f)
				else (i' - 1, combine 0 i' (-x) . f)
		height = vlength m
		stagger' :: (Fractional a, Ord a, IsNatural m) => a -> Matrix n m a -> Maybe (Matrix n m a)
		stagger' _ VZero = Just VZero
		stagger' err' (x :. xs) = do
			(j, h) <- firstNotNull err' 0 x
			guard $ j < height
			let
				x' = recip h ..* x
				m' = x' :. xs
				doZero = zeroBelow err' m' j
			case doZero m' of
				x'' :. VZero -> return $ x'' :. VZero
				x'' :. xs' -> do
					next <- stagger' err' xs'
					return $ x'' :. next

data VecErr n a = VecErr { getErr :: a, getVec :: Vec n a }

toVecErr :: a -> Vec n a -> VecErr n a
toVecErr = VecErr

instance (Num a, Ord a, Eq a, IsNatural n) => Eq (VecErr n a) where
	(VecErr err1 v1) == (VecErr err2 v2) = vfoldr (&&) True . fmap (<err) $ abs (v1 - v2) 
		where err = max err1 err2

instance (Ord a, Num a, IsNatural n) => Ord (VecErr n a) where
	compare (VecErr err1 v1) (VecErr err2 v2) = compare v1fnn v2fnn
		where
			err = max err1 err2
			len = vlength v1
			fnn v = case firstNotNull err 0 v of
				Nothing -> len + 1
				Just (j, _) -> j
			v1fnn = fnn v1
			v2fnn = fnn v2

sortWithErr :: (Ord a, Num a, IsNatural m, IsNatural n) => a -> Matrix n m a -> Maybe (Matrix n m a)
sortWithErr err = fromList . fmap getVec . L.sort . fmap (VecErr err) . toList
				


-- Função de a^m em b^n
type Fn m n a b = Vec n (Vec m a -> b)
type RxR m n = Fn m n Double Double
type R2xR2 = RxR T2 T2
type R3xR3 = RxR T3 T3

-- Aplicação de função
(.$) :: (IsNatural n) => Fn m n a b -> Vec m a -> Vec n b
fs .$ x = fmap ($x) fs

-- Jacobiana para função de a^m em b^n
type Jacobian m n a b = Matrix m n (Vec m a -> b)
type JRxR m n = Jacobian m n Double Double
type JR2xR2 = JRxR T2 T2
type JR3xR3 = JRxR T3 T3

-- Aplicação de jacobiana
(.$$) :: (IsNatural n) => Jacobian m n a b -> Vec m a -> Matrix m n b
j .$$ x = fmap (fmap ($x)) j
